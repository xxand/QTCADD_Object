#include "widget.h"
#include "ui_widget.h"

#include <QStandardItemModel>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    QStandardItemModel *model=new QStandardItemModel();
    ui->tableView->setModel(model);
    //行头
    model->setHorizontalHeaderItem(0,new QStandardItem("www"));
    model->setHorizontalHeaderItem(1,new QStandardItem("http"));

    //列头
    model->setVerticalHeaderItem(0,new QStandardItem("一"));

    //数据：
    model->setItem(0,0,new QStandardItem("第一行，第一列"));
      model->setItem(1,0,new QStandardItem("第2行，第一列"));
    model->setItem(0,1,new QStandardItem("第一行，第2列"));



}

Widget::~Widget()
{
    delete ui;
}




